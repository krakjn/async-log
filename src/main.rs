use std::fs::File;
use std::io::{self, Write};
use std::sync::{Arc, Mutex};
use tracing::{debug, error, info, instrument, warn};
use tracing_subscriber::{fmt::format::FmtSpan, FmtSubscriber};

struct FileWriter(Arc<Mutex<File>>);

impl Write for FileWriter {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let mut file = self.0.lock().unwrap();
        file.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        let mut file = self.0.lock().unwrap();
        file.flush()
    }
}

fn setup_tracing(file: File) {
    // Initialize the tracing subscriber with a formatted output
    let arc_file = Arc::new(Mutex::new(file));
    let subscriber = FmtSubscriber::builder()
    .with_span_events(FmtSpan::CLOSE)
    .with_writer(move || FileWriter(Arc::clone(&arc_file)))
        .finish();

    // Set the subscriber as the default for the application
    tracing::subscriber::set_global_default(subscriber).expect("Failed to set tracing subscriber");
}

#[instrument(name = "alpha", skip_all)]
async fn alpha(arg: Option<i32>) {
    info!("alpha starting");
    debug!("This is a debug message");
    info!("This is an info message");
    warn!("This is a warning message");
    error!("This is an error message");
    info!("alpha shutting down");
}

#[instrument(name = "bravo", skip_all)]
async fn bravo() {
    info!("bravo starting");
    debug!("This is a debug message");
    info!("This is an info message");
    warn!("This is a warning message");
    error!("This is an error message");
    info!("bravo shutting down");
}

#[tokio::main]
async fn main() {
    let file = File::create("async.log").expect("Failed to create file");

    setup_tracing(file);

    let a_handle = tokio::spawn(alpha(None));
    let b_handle = tokio::spawn(bravo());

    if let Err(e) = a_handle.await {
        eprintln!("{}", e);
    }

    if let Err(e) = b_handle.await {
        eprintln!("{}", e);
    }
}
